import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import './App.css';
import Myhome from './Home';


function App() {
  return (
    
    <>
      <Router>
        <Routes>

          <Route path='/home' element={<Myhome />} />

        </Routes>
      </Router>
    </>
  );
}

export default App;
